
#[Innostarter 1st Edition 2020](https://innostarter.gitlab.io/2020/class_1/students/)

#[Innostarter 2nd and 3rd Edition 2021](https://innostarter.gitlab.io/2021/class_2_3/students/)

#[Innostarter 4th Edition 2022](https://innostarter.gitlab.io/2022/class_4/students/)

#[Innostarter 5th Edition 2022](https://innostarter.gitlab.io/2022/class_5/students/)

<!--Innostarter 2020 Class A Summary

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRT3tZYRhSjgd_QWTP8TJkAXwuotwL5vaI5dfO6PNAbBaMU2_NI5IooXtl2m5NrVfxEnuKq4vh4T-zN/embed?start=true&loop=true&delayms=3000" frameborder="0" width="1200" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

#Innostarter 2021 Class B Summary

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQYlr_aRcGsIj9Aw1HuEalwasO_vqpN-XJcwk_WYVNThnpEGts1IQgodObrTLa2TsPuKGUABUxQZP9v/embed?start=true&loop=true&delayms=3000" frameborder="0" width="1200" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

-->
